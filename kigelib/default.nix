{ inputs, pkgs, ... }:
let
  kigelib = (import ./default.nix) { inherit inputs pkgs; };
  outputs = inputs.self.outputs;
in rec {

  # ========================== Buildables ========================== #

  mkSystem = config:
    inputs.nixpkgs.lib.nixosSystem {
      specialArgs = { inherit inputs kigelib; };
      modules = [ config ] ++ outputs.nixosModules.default;
    };

  mkHome = config:
    inputs.home-manager.lib.homeManagerConfiguration {
      pkgs = inputs.nixpkgs.legacyPackages."x86_64-linux";
      extraSpecialArgs = { inherit inputs kigelib; };
      modules = [ config ] ++ outputs.homemanagerModules.default;
    };
  
  colors = {
    bg = stylixConf.base16Scheme.base00;
    bglight = stylixConf.base16Scheme.base01;
    bglighter = stylixConf.base16Scheme.base02;
    bglightest = stylixConf.base16Scheme.base03;
    fgdarker = stylixConf.base16Scheme.base04;
    fg = stylixConf.base16Scheme.base05;
    fglight = stylixConf.base16Scheme.base06;
    fglighter = stylixConf.base16Scheme.base07;
    red = stylixConf.base16Scheme.base08;
    orange = stylixConf.base16Scheme.base09;
    yellow = stylixConf.base16Scheme.base0A;
    green = stylixConf.base16Scheme.base0B;
    cyan = stylixConf.base16Scheme.base0C;
    blue = stylixConf.base16Scheme.base0D;
    purple = stylixConf.base16Scheme.base0E;
    brown = stylixConf.base16Scheme.base0F;
  };

  # stylix config here because it's used in home-manager and nixos
  stylixConf = {
    enable = true;
    polarity = "dark";
    image = "${inputs.wallpaper}";
    base16Scheme = {
      # base00 = "272e33"; # bg0,        Default background
      # base01 = "2e383c"; # bg1,        Lighter background
      # base02 = "414b50"; # bg3,        Selection background
      # base03 = "859289"; # grey1,      Comments
      # base04 = "9da9a0"; # grey2,      Dark foreground
      # base05 = "d3c6aa"; # fg,         Default foreground
      # base06 = "e4e1cd"; # bg3,        Light foreground
      # base07 = "fdf6e3"; # bg0,        Light background
      # base08 = "e67e80"; # red
      # base09 = "e69875"; # orange
      # base0A = "dbbc7f"; # yellow
      # base0B = "a7c080"; # green
      # base0C = "83c092"; # cyan
      # base0D = "7fbbb3"; # blue
      # base0E = "d699b6"; # purple
      # base0F = "4C3743"; # bg_visual
      base00 = "202020"; # bg
      base01 = "2a2827"; # bg2
      base02 = "504945"; # bg6
      base03 = "5a524c"; # bg8
      base04 = "bdae93"; # p-fg3
      base05 = "fbf1c7"; # fg
      base06 = "fbf1c7"; # p-fg1
      base07 = "fbf1c7"; # p-fg0
      base08 = "ea6962"; # red
      base09 = "e78a4e"; # orange
      base0A = "d8a657"; # yellow
      base0B = "a9b665"; # green
      base0C = "89b482"; # aqua/cyan
      base0D = "7daea3"; # blue
      base0E = "d3869b"; # purple
      base0F = "bd6f3e"; # dim-orange
    };
    fonts = {
      serif = {
        package = pkgs.noto-fonts;
        name = "Noto Serif";
      };
      sansSerif = {
        package = pkgs.noto-fonts;
        name = "Noto Sans";
      };
      monospace = {
        package = pkgs.nerdfonts.override { fonts = [ "JetBrainsMono" "NerdFontsSymbolsOnly" ]; };
        name = "JetBrainsMono Nerd Font Mono";
        # package = pkgs.nerdfonts.override {
        #   fonts = [ "0xProto" "NerdFontsSymbolsOnly" ];
        # };
        # name = "0xProto Nerd Font Mono";
      };
      emoji = {
        package = pkgs.noto-fonts-emoji;
        name = "Noto Color Emoji";
      };
      sizes = {
        applications = 12;
        desktop = 12;
        popups = 12;
        terminal = 14;
      };
    };
    opacity.terminal = 0.8;
    cursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 20;
    };
  };
}
