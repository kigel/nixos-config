{ config, pkgs, inputs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.kernelParams = [ "intel_idle.max_cstate=1" ];

  networking.hostName = "nixos-laptop";

  kigel = {
    desktop = {
      enable = false;
      type = "sddm";
      user = "kigel";
    };
    wm.hyprland.enable = true;
  };

  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };


  # networking.wireless.enable = true;
  # networking.wireless.userControlled.enable = true;
  # networking.wireless.networks."Kigel 1" = {
  #     psk = "0507928177";
  # };
  networking.networkmanager.enable = true;

  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot
  services.blueman.enable = true;

  # services.logind.lidSwitch = "ignore";
  # services.logind.lidSwitchDocked = "ignore";
  # services.logind.lidSwitchExternalPower = "ignore";
  # services.logind.extraConfig = ''
  # HandleLidSwitch=suspend
  # HandleLidSwitchExternalPower=ignore
  # HandleLidSwitchDocked=ignore
  # '';

  # DON'T CHANGE
  system.stateVersion = "24.05";
}
