{ pkgs, outputs, ... }:

{
  home.username = "kigel";
  home.homeDirectory = "/home/kigel";
  home.stateVersion = "23.11";

  nixpkgs.config.allowUnfree = true;

  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions; [ asvetliakov.vscode-neovim ];
  };

  kigelHome.environments = {
    hyprland = {
      enable = true;
      monitors = [ "auto, 1920x1080, 0x0, 1" ];
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
