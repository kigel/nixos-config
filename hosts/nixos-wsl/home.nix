{ pkgs, ... }:

{
  home.username = "kigel";
  home.homeDirectory = "/home/kigel";
  home.stateVersion = "23.11";

  nixpkgs.config.allowUnfree = true;

  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions; [ asvetliakov.vscode-neovim ];
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
