{ pkgs, outputs, ... }:

{
  home.username = "kigel";
  home.homeDirectory = "/home/kigel";
  home.stateVersion = "23.11";

  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [
    floorp
  ];

  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions; [ asvetliakov.vscode-neovim ];
  };

  kigelHome.environments = {
    hyprland = {
      monitors =
        [ "DVI-D-1, 1920x1080@144, 0x0, 1" "HDMI-A-1, 1920x1090, -1920x0, 1" ];
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
