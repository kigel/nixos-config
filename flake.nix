{
  description = "Nixos config flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    # nixos-wsl.url = "github:nix-community/NixOS-WSL/2311.5.3";

    home-manager.url = "github:nix-community/home-manager/release-24.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    stylix.url = "github:danth/stylix";

    ags.url = "github:Aylur/ags";

    yazi-nvim.url = "github:mikavilpas/yazi.nvim";
    yazi-nvim.flake = false;

    # hyprland.url = "git+https://github.com/hyprwm/Hyprland/?submodules=1";

    # wallpaper.url =
    #   "https://raw.githubusercontent.com/AngelJumbo/gruvbox-wallpapers/main/wallpapers/irl/houseonthesideofalake.jpg";
    wallpaper.url =
      "https://gruvbox-wallpapers.pages.dev/wallpapers/irl/forest-mountain-cloudy-valley.png";
    wallpaper.flake = false;

    dwm-kigel.url = "gitlab:kigel/dwm";
    dwm-kigel.flake = false;

    dmenu-kigel.url = "gitlab:kigel/dmenu";
    dmenu-kigel.flake = false;

    nur.url = "github:nix-community/nur";
  };

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = inputs.nixpkgs.legacyPackages.${system};
      kigelib = import ./kigelib { inherit inputs pkgs nixpkgs; };
    in {
      nixosModules.default =
        [ ./modulesNixos inputs.stylix.nixosModules.stylix ];
      homemanagerModules.default =
        [ ./modulesHome inputs.stylix.homeManagerModules.stylix ];
      homemanagerAsModuleModules.default = [ ./modulesHome ];

      nixosConfigurations = {
        nixos-pc = kigelib.mkSystem ./hosts/nixos-pc/configuration.nix;
        nixos-laptop = kigelib.mkSystem ./hosts/nixos-laptop/configuration.nix;
      };
      homeConfigurations = {
        "kigel@nixos-pc" = kigelib.mkHome ./hosts/nixos-pc/home.nix;
        "kigel@nixos-laptop" = kigelib.mkHome ./hosts/nixos-laptop/home.nix;
      };
    };
}
