{ lib, config, pkgs, ... }:

let cfg = config.kigel.gaming;
in {
  options.kigel.gaming.enable = lib.mkEnableOption "enable gaming";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      prismlauncher
      # dotnetCorePackages.sdk_6_0_1xx
      dotnetCorePackages.sdk_8_0_1xx
    ];

    programs.steam = { enable = true; };
  };
}
