{ lib, kigelib, config, inputs, ... }:

let cfg = config.kigel.hm-module;
in {
  options.kigel.hm-module = {
    enable = lib.mkEnableOption "enable homemanager nixos module";
    hmcfg = lib.mkOption {
      description = ''
        the config for home-manager
      '';
    };
  };

  imports = [ inputs.home-manager.nixosModules.home-manager ];
  config = lib.mkIf cfg.enable {

    home-manager = {
      extraSpecialArgs = { inherit inputs kigelib; };
      sharedModules = inputs.self.outputs.homemanagerAsModuleModules.default;
      users = { kigel = cfg.hmcfg; };
    };
  };
}
