{ lib, config, pkgs, ... }:

let cfg = config.kigel.pkgs.desktop;
in {
  options.kigel.pkgs.desktop.enable =
    lib.mkEnableOption "enable desktop packages";

  config = lib.mkIf cfg.enable {
    programs._1password-gui.enable = true;
    programs._1password-gui.polkitPolicyOwners = [ "kigel" ];

    environment.systemPackages = with pkgs; [
      # Desktop
      gimp
      obs-studio
      pulsemixer
      vlc
      qbittorrent
      gnome.file-roller
    ];
  };
}
