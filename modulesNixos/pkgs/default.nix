{
  imports = [
    ./essentials.nix
    ./desktop.nix
  ];

  nixpkgs.config.allowUnfree = true;
}
