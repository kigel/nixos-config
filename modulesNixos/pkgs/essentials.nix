{ lib, config, pkgs, ... }:

let cfg = config.kigel.pkgs.essentials;
in {
  options.kigel.pkgs.essentials.enable =
    lib.mkEnableOption "enable essential packages";

  config = lib.mkIf cfg.enable {
    programs._1password.enable = true;

    environment.systemPackages = with pkgs; [
      # CLI Tools
      libnotify
      nix-index
      fzf
      eza
      zoxide
      htop-vim
      unzip
      unrar
      dconf
      fastfetch
      ripgrep
      gnome.file-roller

      # Dev
      rustc
      cargo
      gcc
      openssl
      mongodb-compass
      git
      delve
      go
    ];
  };
}
