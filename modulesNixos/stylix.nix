{ lib, kigelib, config, ... }:

let cfg = config.kigel.stylix;
in {
  options.kigel.stylix.enable = lib.mkEnableOption "enable stylix";

  config = lib.mkIf cfg.enable {
    stylix = lib.mkMerge [
      ({
        autoEnable = false;
        targets = {
          gtk.enable = true;
          grub.enable = true;
          gnome.enable = true;
          chromium.enable = true;
          console.enable = true;
          plymouth.enable = true;
        };
      })
      (kigelib.stylixConf)
    ];
  };
}
