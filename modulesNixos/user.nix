{ lib, config, pkgs, ... }:

let cfg = config.kigel.user;
in {
  options.kigel.user.enable = lib.mkEnableOption "enable user";

  config = lib.mkIf cfg.enable {
    users.users.kigel = {
      isNormalUser = true;
      extraGroups = [ "networkmanager" "docker" "wheel" ];
      initialPassword = "3250";
      shell = pkgs.zsh;
    };
    programs.zsh.enable = true;
  };
}
