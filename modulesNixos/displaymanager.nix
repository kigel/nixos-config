{ lib, config, pkgs, ... }:

let cfg = config.kigel.displaymanager;
in {
  options.kigel.displaymanager = {
    enable = lib.mkEnableOption "configure a display manager";
    type = lib.mkOption {
      description = ''
        sddm or gdm or greetd (leave empty for no display manager)
      '';
    };
    cmd = lib.mkOption {
      description = ''
        default de/wm
      '';
    };
    user = lib.mkOption {
      description = ''
        default username
      '';
    };
  };

  config = lib.mkIf cfg.enable (lib.mkMerge [
    ({
      services = {
        xserver.displayManager.startx.enable = true;
        xserver.enable = true;
        libinput.enable = true;
        libinput.mouse.accelProfile = "flat";
      };
    })

    (lib.mkIf (cfg.type == "sddm") {
      services = {
        displayManager = {
          sddm.enable = true;
          sddm.wayland.enable = true;
          defaultSession = cfg.cmd;
          # autoLogin = {
          #   enable = false;
          #   user = cfg.user;
          # };
        };
      };
    })

    (lib.mkIf (cfg.type == "gdm") {
      services.xserver = {
        displayManager.gdm.enable = true;
      };
    })

    (lib.mkIf (cfg.type == "greetd") {
      services.greetd = {
        enable = true;
        settings = {
          default_session = {
            command =
              "${pkgs.greetd.tuigreet}/bin/tuigreet --remember --remember-session --time";
            user = cfg.user;
          };
          initial_session = {
            command = cfg.cmd;
            user = cfg.user;
          };
        };
      };
    })

  ]);
}
