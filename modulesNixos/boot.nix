{ lib, config, ... }:

let cfg = config.kigel.boot;
in {
  options.kigel.boot.enable = lib.mkEnableOption "enable boot options";

  config = lib.mkIf cfg.enable {
    boot.kernelParams = [ "quiet" ];
    boot.loader = {
      efi = { canTouchEfiVariables = true; };
      # systemd-boot.enable = true;
      grub = {
        enable = true;
        useOSProber = true;
        efiSupport = true;
        device = "nodev";
      };
    };
    boot.plymouth = {
      enable = true;
    };
  };
}
