{ lib, config, inputs, ... }:

let cfg = config.kigel.general;
in {
  options.kigel.general.enable = lib.mkEnableOption "enable general";

  config = lib.mkIf cfg.enable {
    services.gvfs.enable = true;

    environment.variables = rec {
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME = "$HOME/.local/share";
      XDG_STATE_HOME = "$HOME/.local/state";
      XDG_BIN_HOME = "$HOME/.local/bin";
      NIXOS_CONFIG = "/etc/nixos";
      PATH = "${XDG_BIN_HOME}";
    };

    programs.nix-ld = { enable = true; };

    nix.settings.experimental-features = [ "nix-command" "flakes" ];

    security.polkit.enable = true;

    time.timeZone = "Asia/Jerusalem";

    i18n.defaultLocale = "en_US.UTF-8";

    i18n.extraLocaleSettings = {
      LC_ADDRESS = "en_IL";
      LC_IDENTIFICATION = "en_IL";
      LC_MEASUREMENT = "en_IL";
      LC_MONETARY = "en_IL";
      LC_NAME = "en_IL";
      LC_NUMERIC = "en_IL";
      LC_PAPER = "en_IL";
      LC_TELEPHONE = "en_IL";
      LC_TIME = "en_IL";
    };

    services.printing.enable = true;

    sound.enable = true;
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    # fonts = {
    #   packages = with pkgs; [
    #     noto-fonts
    #     noto-fonts-cjk
    #     noto-fonts-color-emoji
    #     jetbrains-mono
    #     meslo-lg
    #     nerdfonts
    #   ];
    #
    #   fontconfig = {
    #     defaultFonts = {
    #       sansSerif = [ "Noto Sans" "Noto Sans Hebrew" ];
    #       serif = [ "Noto Sans" "Noto Sans Hebrew" ];
    #       monospace = [ "JetBrains Mono" ];
    #     };
    #     hinting = { style = "slight"; };
    #   };
    # };
  };
}
