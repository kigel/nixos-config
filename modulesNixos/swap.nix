{ lib, config, ... }:

let cfg = config.kigel.swap;
in {
  options.kigel.swap.enable = lib.mkEnableOption "enable swap";

  config = lib.mkIf cfg.enable {
    swapDevices = [{
      device = "/var/lib/swapfile";
      size = 16 * 1024;
    }];
  };
}
