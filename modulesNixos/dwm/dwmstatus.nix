{ pkgs }:

pkgs.writeShellScriptBin "dwmstatus" ''

  time=""
  volume=""
  battery=""
  connection=""
  lang=""

  blue="#7daea3"
  yellow="#d8a657"
  magenta="#d3869b"
  green="#a9b665"
  cyan="#89b482"
  red="#ea6962"

  update_time () {
    icon="^c$yellow^󱑂^d^"
    time="$icon $(date '+%H:%M %d-%m %a')"
  }

  update_volume () {
    icon="^c$magenta^󰕾^d^"
    volume="$icon $(${pkgs.wireplumber}/bin/wpctl get-volume @DEFAULT_AUDIO_SINK@)"
  }

  update_battery () {
    # check if is laptop
    battery_err=$(cat /sys/class/power_supply/BAT0/capacity 2>&1 1>/dev/null)

    if [ "$battery_err" = "" ]; then
      battery=$(cat /sys/class/power_supply/BAT0/capacity)
    else
      # if is not laptop then just exit and do nothing
      return 0
    fi

    status=$(cat /sys/class/power_supply/BAT0/status)

    echo_battery() {
      if [ "$battery" -gt "$1" ]; then
        if [ "$status" = "Discharging" ]; then
          battery="^c$green^$2^d^ $battery"
          return 0
        fi
        battery="^c$green^$3^d^ $battery"
        return 0
      fi
      return 1
    }

    echo_battery 99 󰁹 󰂅 && return 0
    echo_battery 89 󰂂 󰂋 && return 0
    echo_battery 79 󰂁 󰂊 && return 0
    echo_battery 69 󰂀 󰢞 && return 0
    echo_battery 59 󰁿 󰂉 && return 0
    echo_battery 49 󰁾 󰢝 && return 0
    echo_battery 39 󰁽 󰂈 && return 0
    echo_battery 29 󰁼 󰂇 && return 0
    echo_battery 19 󰁻 󰂆 && return 0
    echo_battery 9 󰁺 󰢜 && return 0
    echo_battery 0 󰂎 󰢟 && return 0
  }

  update_connection () {
    connection_status=$(${pkgs.networkmanager}/bin/nmcli -t general status | awk -F: '{print $1}')

    if [ "$connection_status" = "disconnected" ]; then
      connection="^c$red^󰖪^d^ DISCONNECTED"
    fi
  }

  update_lang () {
    y=$(${pkgs.xorg.setxkbmap}/bin/setxkbmap -query | awk '/layout:/ {print $2}')

    # Convert to uppercase using tr
    y_upper=$(echo "$y" | tr '[:lower:]' '[:upper:]')

    if [ "$y_upper" = "IL" ]; then
      y_upper="HE"
    fi

    icon="^c$blue^^d^"
    lang="$icon $y_upper"
  }

  setroot () {
    laptop_blocks=""
    checklaptop && laptop_blocks="$battery "
  	${pkgs.xorg.xsetroot}/bin/xsetroot -name "$connection $lang $laptop_blocks$volume $time "
  }

  checklaptop () {
    [[ "$(cat /sys/class/power_supply/BAT0/capacity 2>&1 1>/dev/null)" == "" ]] && return 0 || return 1
  }

  while true; do
    update_time
    update_volume
    update_battery
    # update_connection
    update_lang
  	setroot
  	sleep 0.1
  done
''
