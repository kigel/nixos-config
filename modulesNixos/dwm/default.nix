{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.kigel.wm.dwm;
  useLocalSrc = false;
  dwmSrc = inputs.dwm-kigel;
  dmenuSrc = inputs.dmenu-kigel;
in {
  options.kigel.wm.dwm.enable = lib.mkEnableOption "enable dwm";

  config = lib.mkIf cfg.enable {
    services.xserver.displayManager.setupCommands = ''
      ${pkgs.xorg.xrandr}/bin/xrandr --output DVI-D-0 --mode 1920x1080 --rate 144 --output HDMI-0 --mode 1920x1080 --rate 60 --left-of DVI-D-0
    '';
    services.xserver.windowManager.dwm.enable = true;
    services.xserver.windowManager.dwm.package = pkgs.dwm.overrideAttrs
      (oldAttrs: {
        src = if useLocalSrc then
          builtins.path { path = /home/kigel/Repos/dwm; }
        else
          dwmSrc;
        buildInputs = (oldAttrs.buildInputs or [ ])
          ++ [ pkgs.fribidi pkgs.xorg.xcbutil ];
        nativeBuildInputs = (oldAttrs.nativeBuildInputs or [ ])
          ++ [ pkgs.pkg-config ];
      });

    nixpkgs.overlays = [
      (final: prev: {
        dmenu = prev.dmenu.overrideAttrs (old: { src = dmenuSrc; });
      })
    ];

    # essential scripts to run dwm
    environment.systemPackages = [
      pkgs.xdg-desktop-portal
      pkgs.xdg-desktop-portal-gtk
      pkgs.dmenu
      pkgs.xclip
      (import ./dwmstatus.nix { inherit pkgs; })
      # keyboard layout switch script
      (pkgs.writeShellScriptBin "dwm_switchkeyboard" ''
        ${pkgs.xorg.setxkbmap}/bin/setxkbmap -query | grep -q 'il' && ${pkgs.xorg.setxkbmap}/bin/setxkbmap us || ${pkgs.xorg.setxkbmap}/bin/setxkbmap il
      '')
      # volume changing script
      (pkgs.writeShellScriptBin "dwm_volume" ''
        case "$1" in
          "up") ${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+ ;;
          "down") ${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%- ;;
          "mute") ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle ;;
        esac
      '')
      # music controls script
      (pkgs.writeShellScriptBin "dwm_music" ''
        case "$1" in
          "next") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot" next >/dev/null 2>&1 ;;
          "previous") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot" previous >/dev/null 2>&1 ;;
          "play") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot" play-pause >/dev/null 2>&1 ;;
        esac
      '')
      # screenshot script
      (pkgs.writeShellScriptBin "dwm_screenshot" ''
        cur_date=$(date "+%y-%m-%d_%h-%m-%N")
        path="$HOME/Screenshots/$cur_date.png"

        mkdir $HOME/Screenshots 2>/dev/null

        case "$1" in
        full) ${pkgs.scrot}/bin/scrot -M 0 -e "${pkgs.xclip}/bin/xclip -selection clipboard -t image/png -i $f" -F "$path" ;;
        sel) ${pkgs.scrot}/bin/scrot -s -e "${pkgs.xclip}/bin/xclip -selection clipboard -t image/png -i $f" -F "$path" ;;
        esac
      '')
      # dmenu shutdown script
      (pkgs.writeShellScriptBin "dwm_dmenushutdown" ''
        res=$(printf "Shutdown\nReboot\nReload" | ${pkgs.dmenu}/bin/dmenu -i -p "Choose:")

        [ "$res" = "Shutdown" ] && shutdown -h now
        [ "$res" = "Reboot" ] && reboot
        [ "$res" = "Reload" ] && pkill dwm
      '')
      # start script
      (pkgs.writeShellScriptBin "dwm_start" ''
        export _JAVA_AWT_WM_NONREPARENTING=1
        dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY &
        ${pkgs.xbanish}/bin/xbanish &
        ${pkgs.picom}/bin/picom &
        ${pkgs.udiskie}/bin/udiskie &
        ${pkgs.lxqt.lxqt-policykit}/bin/lxqt-policykit-agent &
        ${pkgs.feh}/bin/feh --recursive --randomize --no-fehbg --bg-fill '${inputs.wallpaper}' &
        dwmstatus &

        mkdir "$XDG_CACHE_HOME"/dwm 2>/dev/null

        while true; do
            dwm 2>"$XDG_CACHE_HOME"/dwm/dwm.log
        done
      '')
      pkgs.kitty
      (pkgs.writeShellScriptBin "dwm_launchterminal" ''
        kitty
      '')
      pkgs.pcmanfm
      (pkgs.writeShellScriptBin "dwm_launchfm" ''
        pcmanfm
      '')
      pkgs.firefox
      (pkgs.writeShellScriptBin "dwm_launchbrowser" ''
        firefox
      '')
      pkgs.vscode
      (pkgs.writeShellScriptBin "dwm_launcheditor" ''
        code
      '')
      pkgs.spotify
      pkgs.ncspot
      (pkgs.writeShellScriptBin "dwm_launchmusic" ''
        # spotify
        wezterm start -e ncspot
      '')
      pkgs.discord
      (pkgs.writeShellScriptBin "dwm_launchchat" ''
        discord
      '')
      pkgs.obsidian
      (pkgs.writeShellScriptBin "dwm_launchnotes" ''
        obsidian
      '')
    ];

    services.xserver.windowManager.session = [{
      name = "dwm";
      start = ''
        dwm_start
      '';
    }];
  };
}
