{ lib, config, pkgs, ... }:

let cfg = config.kigel.docker;
in {
  options.kigel.docker.enable = lib.mkEnableOption "enable docker";

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ docker docker-compose ];

    virtualisation.docker.enable = true;
  };
}
