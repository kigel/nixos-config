{ lib, config, pkgs, ... }:

let cfg = config.kigel.gnome;
in {
  options.kigel.gnome = { enable = lib.mkEnableOption "enable gnome and gdm"; };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      gnomeExtensions.blur-my-shell
      gnomeExtensions.appindicator
      gnomeExtensions.forge
    ];

    services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];

    environment.sessionVariables = {
      MOZ_ENABLE_WAYLAND = 0;
    };

    services.xserver = {
      enable = true;
      desktopManager.gnome.enable = true;
    };
  };
}
