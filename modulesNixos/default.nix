{lib, ...}:
{
  imports = [
    ./pkgs
    ./dwm
    ./hyprland.nix
    ./docker.nix
    ./boot.nix
    ./general.nix
    ./displaymanager.nix
    ./hm-module.nix
    ./user.nix
    ./gaming.nix
    ./swap.nix
    ./stylix.nix
    ./gnome.nix
  ];

  kigel = {
    hm-module = {
      enable = lib.mkDefault false;
    };
    boot.enable = lib.mkDefault true;
    user.enable = lib.mkDefault true;
    displaymanager = {
      enable = lib.mkDefault true;
      type = lib.mkDefault "gdm";
      user = lib.mkDefault "kigel";
    };
    gnome.enable = lib.mkDefault true;
    wm.dwm.enable = lib.mkDefault false;
    wm.hyprland.enable = lib.mkDefault true;
    general.enable = lib.mkDefault true;
    docker.enable = lib.mkDefault true;
    swap.enable = lib.mkDefault true;
    stylix.enable = lib.mkDefault true;
    gaming.enable = lib.mkDefault false;
    pkgs = {
      essentials.enable = lib.mkDefault true;
      desktop.enable = lib.mkDefault true;
    };
  };
}
