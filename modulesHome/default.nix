{ lib, pkgs, inputs, ... }: {
  imports = [
    ./programs/wezterm
    ./programs/kitty
    ./programs/lf
    ./programs/yazi
    ./programs/zoxide
    ./programs/feh
    ./programs/picom
    ./programs/dunst
    ./programs/nvim
    ./programs/shell
    ./programs/omp
    ./programs/tmux
    ./programs/imv
    ./programs/firefox

    ./git.nix
    ./stylix.nix
    ./defaultapps.nix

    ./environments/hyprland
  ];

  nixpkgs = {
    overlays = [ inputs.nur.overlay ];
    config = { allowUnfree = true; };
  };

  kigelHome = {
    defaultApps = {
      enable = lib.mkDefault true;
      terminal = {
        package = lib.mkDefault pkgs.kitty;
        name = lib.mkDefault "kitty";
      };
      browser = {
        package = lib.mkDefault pkgs.firefox;
        name = lib.mkDefault "firefox";
      };
      editor = {
        package = lib.mkDefault pkgs.vscode;
        name = lib.mkDefault "code";
      };
      imageviewer = {
        package = lib.mkDefault pkgs.imv;
        name = lib.mkDefault "imv";
      };
      filemanager = {
        package = lib.mkDefault pkgs.pcmanfm;
        name = lib.mkDefault "pcmanfm";
      };
      musicplayer = {
        package = lib.mkDefault pkgs.spotify;
        name = lib.mkDefault "spotify";
      };
      discord = {
        package = lib.mkDefault pkgs.vesktop;
        name = lib.mkDefault "vesktop";
      };
      notes = {
        package = lib.mkDefault pkgs.obsidian;
        name = lib.mkDefault "obsidian";
      };
    };
    git.enable = lib.mkDefault true;
    stylix.enable = lib.mkDefault true;
  };

  kigelHome.environments = { hyprland.enable = lib.mkDefault true; };

  kigelHome.programs = {
    kitty.enable = lib.mkDefault true;
    yazi.enable = lib.mkDefault true;
    zoxide.enable = lib.mkDefault true;
    nvim.enable = lib.mkDefault true;
    shell.enable = lib.mkDefault true;
    imv.enable = lib.mkDefault true;
    firefox.enable = lib.mkDefault true;
    # Disabled
    feh.enable = lib.mkDefault false;
    picom.enable = lib.mkDefault false;
    lf.enable = lib.mkDefault false;
    wezterm.enable = lib.mkDefault false;
    omp.enable = lib.mkDefault false;
    tmux.enable = lib.mkDefault false;
  };
}
