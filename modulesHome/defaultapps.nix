{ config, lib, ... }:

let cfg = config.kigelHome.defaultApps;
in {
  options.kigelHome.defaultApps = {
    enable = lib.mkEnableOption "";

    terminal = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    browser = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    editor = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    imageviewer = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    filemanager = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    musicplayer = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    discord = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
    notes = {
      package = lib.mkOption { type = lib.types.package; };
      name = lib.mkOption { type = lib.types.str; };
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      cfg.terminal.package
      cfg.filemanager.package
      cfg.browser.package
      cfg.imageviewer.package
      cfg.musicplayer.package
      cfg.discord.package
      cfg.notes.package
    ];

    xdg.mime.enable = true;
    xdg.mimeApps = {
      enable = true;
      defaultApplications = {
        "image/png" = [ "${cfg.imageviewer.name}.desktop" ];
        "image/jpeg" = [ "${cfg.imageviewer.name}.desktop" ];

        "x-scheme-handler/http" = [ "${cfg.browser.name}.desktop" ];
        "x-scheme-handler/https" = [ "${cfg.browser.name}.desktop" ];
        "x-scheme-handler/chrome" = [ "${cfg.browser.name}.desktop" ];
        "application/x-extension-htm" = [ "${cfg.browser.name}.desktop" ];
        "application/x-extension-html" = [ "${cfg.browser.name}.desktop" ];
        "application/x-extension-shtml" = [ "${cfg.browser.name}.desktop" ];
        "application/xhtml+xml" = [ "${cfg.browser.name}.desktop" ];
        "application/x-extension-xhtml" = [ "${cfg.browser.name}.desktop" ];
        "application/x-extension-xht" = [ "${cfg.browser.name}.desktop" ];
        "x-scheme-handler/about" = [ "${cfg.browser.name}.desktop" ];
        "x-scheme-handler/unknown" = [ "${cfg.browser.name}.desktop" ];
        "text/html" = [ "${cfg.browser.name}.desktop" ];
        "text/xml" = [ "${cfg.browser.name}.desktop" ];

        "text/*" = [ "${cfg.editor.name}.desktop" ];
        "text/english" = [ "${cfg.editor.name}.desktop" ];
        "text/plain" = [ "${cfg.editor.name}.desktop" ];
        "text/x-c" = [ "${cfg.editor.name}.desktop" ];
        "text/x-c++" = [ "${cfg.editor.name}.desktop" ];
        "text/x-c++hdr" = [ "${cfg.editor.name}.desktop" ];
        "text/x-c++src" = [ "${cfg.editor.name}.desktop" ];
        "text/x-java" = [ "${cfg.editor.name}.desktop" ];
        "text/x-makefile" = [ "${cfg.editor.name}.desktop" ];
        "text/x-go" = [ "${cfg.editor.name}.desktop" ];

        "inode/directory" = [ "${cfg.filemanager.name}.desktop" ];
      };
    };
  };
}
