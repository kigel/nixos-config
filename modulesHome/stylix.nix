{ config, lib, kigelib, pkgs, ... }:

let cfg = config.kigelHome.stylix;
in {
  options.kigelHome.stylix.enable = lib.mkEnableOption "enable stylix ricing";

  config = lib.mkIf cfg.enable {

    stylix = lib.mkMerge [
      ({
        autoEnable = true;
        targets.waybar.enable = false;
        targets.vscode.enable = false;
        targets.kitty.enable = false;
      })
      (kigelib.stylixConf)
    ];

    gtk = {
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme;
      };
    };

    # Kitty
    programs.kitty.font = {
      inherit (config.stylix.fonts.monospace) package name;
      size = config.stylix.fonts.sizes.terminal;
    };
    programs.kitty.settings = {

      color0 = "#${kigelib.colors.bg}";
      color8 = "#${kigelib.colors.bglightest}";
      color1 = "#${kigelib.colors.red}";
      color9 = "#${kigelib.colors.red}";
      color2 = "#${kigelib.colors.green}";
      color10 = "#${kigelib.colors.green}";
      color3 = "#${kigelib.colors.yellow}";
      color11 = "#${kigelib.colors.yellow}";
      color4 = "#${kigelib.colors.blue}";
      color12 = "#${kigelib.colors.blue}";
      color5 = "#${kigelib.colors.purple}";
      color13 = "#${kigelib.colors.purple}";
      color6 = "#${kigelib.colors.cyan}";
      color14 = "#${kigelib.colors.cyan}";
      color7 = "#${kigelib.colors.fg}";
      color15 = "#${kigelib.colors.fglight}";

      foreground = "#${kigelib.colors.fg}";
      background = "#${kigelib.colors.bg}";
      background_opacity = with config.stylix.opacity;
        "${builtins.toString terminal}";

      tab_bar_background = lib.mkForce "#${kigelib.colors.bg}";
      active_tab_foreground = lib.mkForce "#${kigelib.colors.green}";
      active_tab_background = lib.mkForce "#${kigelib.colors.bg}";
      active_tab_font_style = lib.mkForce "normal";
      inactive_tab_foreground = lib.mkForce "#${kigelib.colors.fg}";
      inactive_tab_background = lib.mkForce "#${kigelib.colors.bg}";
      inactive_tab_font_style = lib.mkForce "normal";
    };
  };
}
