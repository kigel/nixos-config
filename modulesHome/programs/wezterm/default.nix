{ lib, config, ... }:

let
  cfg = config.kigelHome.programs.wezterm;
in
{
  options.kigelHome.programs.wezterm.enable = lib.mkEnableOption "enable wezterm";

  config = lib.mkIf cfg.enable {
    programs.wezterm = {
      enable = true;
      extraConfig = '' ${builtins.readFile ./wezterm.lua} '';
    };
  };
}
