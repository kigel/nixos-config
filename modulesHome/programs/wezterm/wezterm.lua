local wezterm = require("wezterm")
local config = {}

config.enable_wayland = true;
config.front_end = "WebGpu";

-- colorscheme
config.color_scheme = "gruvbox_material_dark_hard"
config.color_schemes = {
	["gruvbox_material_dark_hard"] = {
		foreground = "#D4BE98",
		background = "#1D2021",
		cursor_bg = "#D4BE98",
		cursor_border = "#D4BE98",
		cursor_fg = "#1D2021",
		selection_bg = "#D4BE98",
		selection_fg = "#3C3836",

		ansi = { "#1d2021", "#ea6962", "#a9b665", "#d8a657", "#7daea3", "#d3869b", "#89b482", "#d4be98" },
		brights = { "#928374", "#ea6962", "#a9b665", "#d8a657", "#7daea3", "#d3869b", "#89b482", "#d4be98" },
	},
	["everforest_dark_hard"] = {
		foreground = "#D3C6AA",
		background = "#1E2326",
		cursor_bg = "#D3C6AA",
		cursor_border = "#D3C6AA",
		cursor_fg = "#1E2326",
		selection_bg = "#D3C6AA",
		selection_fg = "#2E383C",

		ansi = { "#1E2326", "#E67E80", "#A7C080", "#DBBC7F", "#7FBBB3", "#D699B6", "#83C092", "#D3C6AA" },
		brights = { "#F2EFDF", "#E67E80", "#A7C080", "#DBBC7F", "#7FBBB3", "#D699B6", "#83C092", "#D3C6AA" },
	},
}

config.window_background_opacity = 0.9

-- bidi config
config.bidi_enabled = true
config.bidi_direction = "LeftToRight"

-- fonts
config.font = wezterm.font_with_fallback({
	-- "monospace",
	{ family = "JetBrains Mono", weight = "Medium" },
})

config.font_size = 14

-- misc
config.cursor_blink_rate = 0
config.enable_tab_bar = true
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.hide_tab_bar_if_only_one_tab = true
config.window_close_confirmation = "NeverPrompt"
config.scrollback_lines = 3500
config.window_padding = {
	left = "1cell",
	right = "1cell",
	top = "0.5cell",
	bottom = "0cell",
}

config.keys = {
	{
		key = "Enter",
		mods = "ALT",
		action = wezterm.action.DisableDefaultAssignment,
	},
}

-- keybind
local act = wezterm.action

config.keys = {
	{ key = "Enter", mods = "ALT", action = act({ SpawnTab = "CurrentPaneDomain" }) },

	{ key = "j", mods = "ALT", action = act({ ActivateTabRelative = -1 }) },
	{ key = "k", mods = "ALT", action = act({ ActivateTabRelative = 1 }) },
	{ key = "j", mods = "ALT|SHIFT", action = act({ MoveTabRelative = -1 }) },
	{ key = "k", mods = "ALT|SHIFT", action = act({ MoveTabRelative = 1 }) },

	{ key = "w", mods = "ALT", action = wezterm.action({ CloseCurrentTab = { confirm = false } }) },

	{ key = "1", mods = "ALT", action = act({ ActivateTab = 0 }) },
	{ key = "2", mods = "ALT", action = act({ ActivateTab = 1 }) },
	{ key = "3", mods = "ALT", action = act({ ActivateTab = 2 }) },
	{ key = "4", mods = "ALT", action = act({ ActivateTab = 3 }) },
	{ key = "5", mods = "ALT", action = act({ ActivateTab = 4 }) },
	{ key = "6", mods = "ALT", action = act({ ActivateTab = 5 }) },
	{ key = "7", mods = "ALT", action = act({ ActivateTab = 6 }) },
	{ key = "8", mods = "ALT", action = act({ ActivateTab = 7 }) },
	{ key = "9", mods = "ALT", action = act({ ActivateTab = 8 }) },
}

local bg_background = config.color_schemes[config.color_scheme].background
local bg_inactive = config.color_schemes[config.color_scheme].background
local fg_inactive = config.color_schemes[config.color_scheme].foreground
local bg_active = config.color_schemes[config.color_scheme].background
local fg_active = config.color_schemes[config.color_scheme].ansi[3]

config.colors = {
	tab_bar = {
		background = bg_background,
		-- The active tab is the one that has focus in the window
		active_tab = {
			-- The color of the background area for the tab
			bg_color = bg_active,
			-- The color of the text for the tab
			fg_color = fg_active,

			-- Specify whether you want "Half", "Normal" or "Bold" intensity for the
			-- label shown for this tab.
			-- The default is "Normal"
			intensity = "Bold",

			-- Specify whether you want "None", "Single" or "Double" underline for
			-- label shown for this tab.
			-- The default is "None"
			underline = "None",

			-- Specify whether you want the text to be italic (true) or not (false)
			-- for this tab.  The default is false.
			italic = false,

			-- Specify whether you want the text to be rendered with strikethrough (true)
			-- or not for this tab.  The default is false.
			strikethrough = false,
		},
		-- Inactive tabs are the tabs that do not have focus
		inactive_tab = {
			bg_color = bg_inactive,
			fg_color = fg_inactive,

			-- The same options that were listed under the `active_tab` section above
			-- can also be used for `inactive_tab`.
		},
		-- You can configure some alternate styling when the mouse pointer
		-- moves over inactive tabs
		inactive_tab_hover = {
			bg_color = bg_inactive,
			fg_color = fg_inactive,
			italic = false,

			-- The same options that were listed under the `active_tab` section above
			-- can also be used for `inactive_tab_hover`.
		},
		-- The new tab button that let you create new tabs
		new_tab = {
			bg_color = bg_background,
			fg_color = bg_inactive,

			-- The same options that were listed under the `active_tab` section above
			-- can also be used for `new_tab`.
		},
		-- You can configure some alternate styling when the mouse pointer
		-- moves over the new tab button
		new_tab_hover = {
			bg_color = bg_background,
			fg_color = bg_inactive,
			italic = false,

			-- The same options that were listed under the `active_tab` section above
			-- can also be used for `new_tab_hover`.
		},
	},
}

return config
