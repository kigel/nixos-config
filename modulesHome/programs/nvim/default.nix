{ inputs, lib, config, pkgs, ... }:

let cfg = config.kigelHome.programs.nvim;
in {
  options.kigelHome.programs.nvim.enable = lib.mkEnableOption "enable neovim";

  config = lib.mkIf cfg.enable {
    nixpkgs = {
      overlays = [
        (final: prev: {
          vimPlugins = prev.vimPlugins // {
            yazi-nvim = prev.vimUtils.buildVimPlugin {
              name = "yazi.nvim";
              src = inputs.yazi-nvim;
            };
          };
        })
      ];
    };

    programs.neovim = {
      enable = true;

      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;

      extraPackages = with pkgs; [
        # LSP(s)
        lua-language-server
        nil
        gopls
        clang
        rust-analyzer
        nodePackages.typescript-language-server

        # Formatters
        shfmt
        prettierd
        goimports-reviser
        nixfmt-classic
        black
        stylua
        rustfmt

        # DAP(s)
        delve
        lldb_17

        ripgrep
      ];

      plugins = with pkgs.vimPlugins; [
        # Telescope And File Managers
        telescope-nvim
        telescope-fzf-native-nvim
        nvim-web-devicons
        yazi-nvim

        # CMP
        nvim-cmp
        luasnip
        cmp_luasnip
        cmp-nvim-lsp
        friendly-snippets

        # Lualine
        lualine-nvim

        # Comment
        comment-nvim

        # LSP
        nvim-lspconfig
        fidget-nvim
        neodev-nvim

        # DAP
        nvim-dap
        nvim-dap-ui
        nvim-dap-virtual-text
        nvim-dap-go
        nvim-dap-python

        # Git
        vim-fugitive
        gitsigns-nvim

        # Format
        conform-nvim

        # Theme
        gruvbox-material

        # Colors
        nvim-colorizer-lua

        # Line on indents
        indent-blankline-nvim

        # Sudo Support
        vim-suda

        # Treesitter
        (nvim-treesitter.withPlugins (p: [
          p.tree-sitter-nix
          p.tree-sitter-vim
          p.tree-sitter-lua
          p.tree-sitter-bash
          p.tree-sitter-python
          p.tree-sitter-go
          p.tree-sitter-json
          p.tree-sitter-c
          p.tree-sitter-cpp
          p.tree-sitter-typescript
          p.tree-sitter-javascript
          p.tree-sitter-html
          p.tree-sitter-css
          p.tree-sitter-toml
          p.tree-sitter-yaml
          p.tree-sitter-git_config
          p.tree-sitter-rust
        ]))
        nvim-treesitter-textobjects
      ];

      extraLuaConfig = ''
        -- Base Settings
        ${builtins.readFile ./config/core/options.lua}
        ${builtins.readFile ./config/core/keymaps.lua}

        -- Plugins
        ${builtins.readFile ./config/plugins/start.lua}
        ${builtins.readFile ./config/plugins/treesitter.lua}
        ${builtins.readFile ./config/plugins/cmp.lua}
        ${builtins.readFile ./config/plugins/comment.lua}
        -- ${builtins.readFile ./config/plugins/lualine.lua}
        ${builtins.readFile ./config/plugins/lsp.lua}
        ${builtins.readFile ./config/plugins/format.lua}
        ${builtins.readFile ./config/plugins/git.lua}
        ${builtins.readFile ./config/plugins/indent_lines.lua}
        ${builtins.readFile ./config/plugins/telescope.lua}
        ${builtins.readFile ./config/plugins/yazi.lua}
        ${builtins.readFile ./config/plugins/theme.lua}
        ${builtins.readFile ./config/plugins/colorizer.lua}
        ${builtins.readFile ./config/plugins/dap.lua}
        ${builtins.readFile ./config/plugins/end.lua}
      '';
    };
  };
}

