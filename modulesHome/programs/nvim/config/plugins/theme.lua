vim.o.background = "dark"

vim.g.gruvbox_material_foreground = "material"
vim.g.gruvbox_material_transparent_background = 1
vim.g.gruvbox_material_float_style = "bright"
vim.cmd.colorscheme("gruvbox-material")
