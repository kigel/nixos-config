local yazi = require("yazi")
yazi.setup({
  open_for_directories = true,
  floating_window_scaling_factor = 0.8,
  yazi_floating_window_border = "none",
})

vim.keymap.set("n", "<C-o>", function() yazi.yazi() end, {})
