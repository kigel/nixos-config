-- local custom_theme = require("lualine.themes.gruvbox-material")
--
-- local background = custom_theme.normal.c.bg
--
-- -- Change the background of lualine_c section for normal mode
-- custom_theme.normal.c.bg = background
-- custom_theme.insert.c.bg = background
-- custom_theme.visual.c.bg = background
-- custom_theme.replace.c.bg = background
-- custom_theme.command.c.bg = background
-- custom_theme.inactive.c.bg = background
--
-- custom_theme.normal.b.bg = background
-- custom_theme.insert.b.bg = background
-- custom_theme.visual.b.bg = background
-- custom_theme.replace.b.bg = background
-- custom_theme.command.b.bg = background
-- custom_theme.inactive.b.bg = background
--
-- require("lualine").setup({
-- 	options = {
-- 		icons_enabled = true,
-- 		theme = custom_theme,
-- 		component_separators = { left = "│", right = "│" },
-- 		section_separators = {},
-- 		globalstatus = true,
-- 	},
-- 	sections = {
-- 		lualine_a = { "mode" },
-- 		lualine_b = {},
-- 		lualine_c = { "filename" },
-- 		lualine_x = {},
-- 		lualine_y = { "searchcount", "branch", "diff", "diagnostics", "filetype", "location" },
-- 		lualine_z = {},
-- 	},
-- })
