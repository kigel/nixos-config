{ lib, config, pkgs, ... }:

let cfg = config.kigelHome.programs.shell;
in {
  options.kigelHome.programs.shell.enable = lib.mkEnableOption "enable shell";

  config = lib.mkIf cfg.enable {
    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      completionInit = " ";
      initExtraFirst = "${builtins.readFile ./zshrc} ";
    };

    home.file = {
      ".config/zsh/plugins/zsh-autosuggestions".source =
        "${pkgs.zsh-autosuggestions}/share/zsh-autosuggestions";
      ".config/zsh/plugins/zsh-fast-syntax-highlighting".source =
        "${pkgs.zsh-fast-syntax-highlighting}/share/zsh/site-functions";
      ".config/zsh/plugins/zsh-vi-mode".source =
        "${pkgs.zsh-vi-mode}/share/zsh-vi-mode";
    };

    home.sessionVariables = {
      TERMINAL = "wezterm";
      OPENER = "xdg-open";
      EDITOR = "nvim";
      VISUAL = "nvim";
      FM = "pcmanfm";
    };

    home.preferXdgDirectories = true;

  };
}

