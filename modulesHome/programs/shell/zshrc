alias bins="$NIXOS_CONFIG/modulesHome/bin"

alias proj="$HOME/Repos"

alias l="exa"
alias la="exa -A"
alias ll="exa -lh"
alias lla="exa -Alh"

alias img="feh -N --keep-zoom-vp -B #282828"

#  _____ _   _ _   _  ____ _____ ___ ___  _   _ ____  
# |  ___| | | | \ | |/ ___|_   _|_ _/ _ \| \ | / ___| 
# | |_  | | | |  \| | |     | |  | | | | |  \| \___ \ 
# |  _| | |_| | |\  | |___  | |  | | |_| | |\  |___) |
# |_|    \___/|_| \_|\____| |_| |___\___/|_| \_|____/ 

homeb () {
  if [[ "$1" == "u" ]]; then
    nix flake update $NIXOS_CONFIG
  fi
  home-manager switch --flake $NIXOS_CONFIG 
}

nixb ()
{
  if [[ "$1" == "u" ]]; then
    nix flake update $NIXOS_CONFIG
  elif [[ "$1" == "t" ]]; then
    sudo nixos-rebuild test --flake $NIXOS_CONFIG
  fi
  sudo nixos-rebuild switch --flake $NIXOS_CONFIG
}

# Config function for NixOS
con () {
  cd $NIXOS_CONFIG
  nvim -c "lua require('telescope.builtin').find_files()" $NIXOS_CONFIG/flake.nix
}

pro () {
  cd $HOME/Repos
  nvim $HOME/Repos
}

checkpkg () {
  level="999"
  if ! [ "$2" -eq "" ]; then
    level="$2"
  fi
  exa --tree --level "$level" "$(nix build nixpkgs#$1 --print-out-paths --no-link)"
}

findfont () {
  fc-list | grep "$1" | awk -F': ' '{print $NF}' | sort
}

# function lfcd ()
# {
#     tmp="$(mktemp -uq)"
#     trap 'rm -f $tmp >/dev/null 2>&1 && trap - HUP INT QUIT TERM PWR EXIT' HUP INT QUIT TERM PWR EXIT
#     lf -last-dir-path="$tmp" "$@"
#     if [ -f "$tmp" ]; then
#         dir="$(cat "$tmp")"
#         [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
#     fi
# }
#
# bindkey -s '^o' '^ulfcd\n'

function yazicd() {
  local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}
bindkey -s '^o' '^uyazicd\n'

#  __  __ ___ ____   ____    ____ ___  _   _ _____ ___ ____
# |  \/  |_ _/ ___| / ___|  / ___/ _ \| \ | |  ___|_ _/ ___|
# | |\/| || |\___ \| |     | |  | | | |  \| | |_   | | |  _
# | |  | || | ___) | |___  | |__| |_| | |\  |  _|  | | |_| |
# |_|  |_|___|____/ \____|  \____\___/|_| \_|_|   |___\____|

setopt autocd nomatch notify
unsetopt beep extendedglob	# Disables beep sound
setopt autocd			# Automatically cd into typed directory.
stty stop undef			# Disable ctrl-s to freeze terminal.

#  ____  ____  _
# |  _ \/ ___|/ |
# | |_) \___ \| |
# |  __/ ___) | |
# |_|   |____/|_|

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
zstyle ':vcs_info:git:*' formats '%b'

# echo newline after each command except from start
precmd() {
  precmd() {
    echo
  }
}
# alias clear so it doesn't echo a newline
alias clear="precmd() {precmd() {echo }} && clear"

PROMPT='%F{green}%n %F{normal}in %F{magenta}%~ %B%F{008}${vcs_info_msg_0_}%b%f
> '

#  _   _ ___ ____ _____ ___  ______   __
# | | | |_ _/ ___|_   _/ _ \|  _ \ \ / /
# | |_| || |\___ \ | || | | | |_) \ V /
# |  _  || | ___) || || |_| |  _ < | |
# |_| |_|___|____/ |_| \___/|_| \_\|_|

if [ ! -d "$XDG_CACHE_HOME/zsh" ]; then
	mkdir "$XDG_CACHE_HOME/zsh"
fi

HISTFILE=$XDG_CACHE_HOME/zsh/histfile
HISTSIZE=500000
SAVEHIST=500000
setopt share_history

#     _   _   _ _____ ___     ____ ___  __  __ ____  _     _____ _____ _____
#    / \ | | | |_   _/ _ \   / ___/ _ \|  \/  |  _ \| |   | ____|_   _| ____|
#   / _ \| | | | | || | | | | |  | | | | |\/| | |_) | |   |  _|   | | |  _|
#  / ___ \ |_| | | || |_| | | |__| |_| | |  | |  __/| |___| |___  | | | |___
# /_/   \_\___/  |_| \___/   \____\___/|_|  |_|_|   |_____|_____| |_| |_____|

autoload -Uz compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=245"

source $XDG_CONFIG_HOME/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
bindkey '^ ' autosuggest-accept

source $XDG_CONFIG_HOME/zsh/plugins/zsh-fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source $XDG_CONFIG_HOME/zsh/plugins/zsh-vi-mode/zsh-vi-mode.plugin.zsh
