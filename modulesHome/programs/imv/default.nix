{ lib, config, pkgs, kigelib, ... }:

let cfg = config.kigelHome.programs.imv;
in {
  options.kigelHome.programs.imv.enable = lib.mkEnableOption "enable imv";

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [ imv ];
    home.file.".config/imv/config".text = ''
      [options]
      background = #${kigelib.colors.bg}
      overlay = true
      overlay_font = sans:14
      overlay_text_color = #${kigelib.colors.fg}
      overlay_background_color = #${kigelib.colors.bg}
      scaling_mode = shrink
      overlay_text = "$imv_current_file $imv_current_index/$imv_file_count"

      suppress_default_binds = true

      [binds]
      l = next
      h = prev
      <Left> = prev
      <Right> = next
      gg = goto 1
      <Shift+G> = goto -1

      k = zoom 5
      j = zoom -5
      <Up> = zoom 5
      <Down> = zoom -5

      <Shift+K> = pan 0 80
      <Shift+J> = pan 0 -80
      <Shift+L> = pan -80 0
      <Shift+H> = pan 80 0

      <Ctrl+r> = rotate by 90

      f = fullscreen
      c = center
      r = reset
      q = quit

      <period> = next_frame
      <space> = toggle_playing
    '';
  };
}

