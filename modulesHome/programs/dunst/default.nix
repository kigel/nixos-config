{ lib, config, ... }:

let cfg = config.kigelHome.programs.dunst;
in {
  options.kigelHome.programs.dunst.enable = lib.mkEnableOption "enable dunst";

  config = lib.mkIf cfg.enable {
    services.dunst = {
      enable = true;
      settings = {
        global = {
          follow = "mouse";
          font = lib.mkForce "monospace";
        };
      };
    };
  };
}
