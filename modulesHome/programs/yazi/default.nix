{ lib, config, pkgs, ... }:

let cfg = config.kigelHome.programs.yazi;
in {
  options.kigelHome.programs.yazi.enable = lib.mkEnableOption "enable yazi";

  config = lib.mkIf cfg.enable {
    programs.yazi = {
      enable = true;
    };
  };
}

