{ lib, config, ... }:

let cfg = config.kigelHome.programs.zoxide;
in {
  options.kigelHome.programs.zoxide.enable = lib.mkEnableOption "enable zoxide";

  config = lib.mkIf cfg.enable {
    home.shellAliases = {
      cd = "z";
    };
    programs.zoxide = {
      enable = true;
    };
  };
}

