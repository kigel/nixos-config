{ lib, config, ... }:

let
  cfg = config.kigelHome.programs.picom;
in
{
  options.kigelHome.programs.picom.enable = lib.mkEnableOption "enable picom";

  config = lib.mkIf cfg.enable {
    services.picom = {
      enable = true;
      activeOpacity = 1.0;
      inactiveOpacity = 1.0;
      menuOpacity = 1.0;
      backend = "glx";
      fade = true;
      fadeDelta = 5;
      fadeSteps = [ 0.03 0.03 ];
      vSync = true;
      shadow = false;
      settings = {
        # blur = {
        #   method = "dual_kawase";
        #   strength = 7;
        # };
        corner-radius = 14;
        rounded-corners-exclude = [
          "window_type = 'dock'"
          "window_type = 'desktop'"
          "window_type = 'dnd'"
          "window_type = 'unknown'"
          "window_type = 'toolbar'"
          "window_type = 'menu'"
          "window_type = 'utility'"
          "window_type = 'splash'"
          "window_type = 'dialog'"
          "window_type = 'normal'"
          "window_type = 'notification'"
          # "window_type = 'combo'"
          # "window_type = 'dropdown_menu'"
          # "window_type = 'popup_menu'"
          "window_type = 'tooltip'"
        ];
      };
    };
  };
}
