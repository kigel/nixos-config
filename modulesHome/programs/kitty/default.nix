{ lib, config, pkgs, ... }:

let cfg = config.kigelHome.programs.kitty;
in {
  options.kigelHome.programs.kitty.enable = lib.mkEnableOption "enable kitty";

  config = lib.mkIf cfg.enable {
    programs.kitty = {
      enable = true;
      keybindings = {
        "alt+enter" = "new_tab_with_cwd";
        "alt+k" = "next_tab";
        "alt+shift+k" = "move_tab_forward";
        "alt+j" = "previous_tab";
        "alt+shift+j" = "move_tab_forward";
        "alt+w" = "close_tab";
      };
      settings = {
        scrollback_lines = 10000;
        enable_audio_bell = false;
        update_check_interval = 0;
        confirm_os_window_close = 0;
        window_padding_width = 7;
        tab_max_length = 15;
        tab_bar_style = "separator";
        tab_separator = " | ";
        tab_bar_margin_width = "7.0";
        tab_bar_margin_height = "7.0 7.0";
        tab_title_template = "[{title}]";
      };
      # extraConfig = ''
      #   modify_font cell_height 1px
      # '';
    };
  };
}

