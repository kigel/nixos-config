{ lib, config, ... }:

let cfg = config.kigelHome.programs.omp;
in {
  options.kigelHome.programs.omp.enable = lib.mkEnableOption "enable Oh-My-Posh";

  config = lib.mkIf cfg.enable {
    programs.oh-my-posh = {
      enable = true;
      # settings = builtins.fromJSON (builtins.unsafeDiscardStringContext (builtins.readFile "${pkgs.oh-my-posh}/share/oh-my-posh/themes/spaceship.omp.json"));
      settings = builtins.fromTOML (builtins.unsafeDiscardStringContext (builtins.readFile ./custom.omp.toml));
    };
  };
}

