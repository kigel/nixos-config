{ lib, kigelib, config, pkgs, ... }:

let cfg = config.kigelHome.programs.waybar;
in {
  options.kigelHome.programs.waybar.enable = lib.mkEnableOption "enable waybar";

  config = lib.mkIf cfg.enable {
    programs.waybar = {
      enable = true;
      settings = {
        mainBar = {
          layer = "top";
          position = "top";
          height = 0;
          spacing = 0;
          modules-left = [ "tray" "wireplumber" "mpris" ];
          modules-center = [ "hyprland/workspaces" ];
          modules-right =
            [ "hyprland/language" "network" "battery" "clock" ];

          "hyprland/workspaces" = {
            all-outputs = true;
            active-only = false;
            format = "{icon}";
            move-to-monitor = true;
            format-icons = {
              "1" = "";
              "2" = "";
              "3" = "";
              "4" = "";
              "5" = "󰭹";
              "6" = "";
            };
          };
          "hyprland/window" = {
            format = "{initialTitle}";
            separate-outputs = true;
            icon = true;
            icon-size = 18;
            max-length = 150;
          };
          "hyprland/language" = {
            format = "<span foreground='#${kigelib.colors.blue}'></span> {}";
            format-en = "EN";
            format-he = "IL";
          };
          "mpris" = {
            player = "spotify_player";
            format =
              "<span foreground='#${kigelib.colors.green}'>{status_icon}</span> {dynamic}";
            status-icons = {
              playing = "";
              paused = "";
              stopped = "";
            };
            dynamic-order = [ "artist" "title" ];
            # dynamic-importance-order = [ "artist" "title" ];
            dynamic-len = 60;
            dynamic-separator = " - ";
          };
          "network" = {
            # interface = "wlp2s0";
            format = "";
            format-wifi = "{essid} ({signalStrength}%) ";
            format-ethernet = "";
            format-disconnected = "NO NETWORK";
            tooltip-format-wifi = "{essid} ({signalStrength}%) ";
            tooltip-format-ethernet = "{ifname} ";
            tooltip-format-disconnected = "";
            max-length = 50;
          };
          "battery" = { };
          "wireplumber" = {
            format = "<span foreground='#${kigelib.colors.cyan}'>󰕾</span> {volume}%";
          };
          "clock" = {
            format = "<span foreground='#${kigelib.colors.yellow}' font_size='12'>󰥔</span> {:%b %m %H:%M}";
            tooltip-format = "{calendar}";
          };
          "tray" = {
            icon-size = 21;
            spacing = 5;
          };
        };
      };
      style = ''
        * {
          font-family: "Symbols Nerd Font Mono", "0xPropo";
          /*font-family: monospace;*/
          font-size: 18px;
          font-weight: normal; /* DONT CHANGE */
        }

        window#waybar {
          background: @theme_base_color;
          border-bottom: 0px;
          padding: 0px 0px;
          outline: 0px;
          border: 0px;
        }

        .modules-left {
          margin: 5px 0;
          margin-left: 5px;
        }
        .modules-center {
          margin: 5px 0;
        }
        .modules-right {
          margin: 5px 0;
          margin-right: 5px;
        }

        .module {
          margin: 2px 5px;
          padding: 0 4px;
        }

        #clock {
          background: @theme_base_color;
          border: none;
        }

        #workspaces button {
          color: shade(@theme_base_color, 2.2);
          margin-right: 2px;
          margin-left: 2px;
        }

        #workspaces button.visible {
          color: @theme_text_color;
        }

        #workspaces button.hosting-monitor.visible {
          color: #${kigelib.colors.purple};
        }
      '';
    };
  };
}

