const options = {
  spacing_between_modules: 0,
  spacing_between_symbol_label: 8,
  notification_timeout: 5,

  workspace_icons: {
    1: "",
    2: "",
    3: "",
    4: "",
    5: "󰭹",
    6: "",
  },

  // workspace_icons: {
  //   1: "",
  //   2: "",
  //   3: "",
  //   4: "",
  //   5: "",
  //   6: "",
  // },
};

export default options;
