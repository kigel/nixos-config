import options from "options"

const time = Variable("", {
  poll: [1000, 'date +"%d %b %H:%M"'],
});
const Time = () =>
  Widget.Box({
    class_names: ["time", "widget"],
    spacing: options.spacing_between_symbol_label,
    children: [
      // Widget.Label({
      //   class_names: ["symbol"],
      //   label: "a",
      // }),
      Widget.Label({
        class_name: "text",
        label: time.bind()
      }),
    ],
  });

export default Time
