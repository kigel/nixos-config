import options from "options";
import { MprisPlayer } from "types/service/mpris";

const mpris = await Service.import("mpris");

function getPlayer(): MprisPlayer | null {
  return mpris.getPlayer("spotify");
}

const Media = () =>
  Widget.Button({
    class_names: ["media", "widget"],
    child: Widget.Box({
      spacing: options.spacing_between_symbol_label,
      children: [
        Widget.Label({
          class_names: ["symbol"],
        }).hook(mpris, (label) => {
          const player: MprisPlayer | null = getPlayer();
          if (!player) {
            return;
          }

          const { play_back_status } = player;
          const icons = {
            Playing: "",
            Paused: "",
            Stopped: "",
          };
          label.toggleClassName("playing", play_back_status === "Playing");
          label.toggleClassName("paused", play_back_status === "Paused");
          label.toggleClassName("stopped", play_back_status === "Stopped");
          label.label = icons.hasOwnProperty(play_back_status)
            ? icons[play_back_status]
            : play_back_status;
        }),
        Widget.Label().hook(mpris, (label) => {
          const player: MprisPlayer | null = getPlayer(); 
          if (!player) {
            return;
          }

          label.class_names = [`text`];
          const { track_artists, track_title } = player;
          label.label = `${track_artists.join(", ")} - ${track_title}`;
        }),
      ],
    }),
  }).hook(mpris, (button) => {
    const player: MprisPlayer | null = getPlayer(); 
    if (!player) {
      button.visible = false;
      return;
    }
    button.visible = true;

    button.on_clicked = () => player.playPause();
  });

// const Players = () =>
//   // Widget.Box({
//   //   children: mpris.bind("players").as((p) => p.map(Player)),
//   // });
//   Widget.Box({
//     child: mpris.bind("players").as((p) => {
//       const player: MprisPlayer | undefined = p.find((pp) => {
//         return pp.name === "spotify";
//       });
//       if (player != undefined) {
//         return Player(player);
//       } else {
//         return Widget.Label({ label: "NO PLAYERS FOUND" });
//       }
//     }),
//   });

export default Media;
