// import options from "options";

const hyprland = await Service.import("hyprland");

const Title = (mon: number) =>
  Widget.Label({
    class_names: ["title", "widget"],
    label: `yo ${mon}`,
    setup: (self) =>
      self.hook(hyprland, () => {
        const active_work: number =
          hyprland.getMonitor(mon)?.activeWorkspace.id || 0;
        const active_window_add: string =
          hyprland.getWorkspace(active_work)?.lastwindow || "";
        const active_window_class: string =
          hyprland.getClient(active_window_add)?.class || "";
        self.set_label(`${active_window_class}`);
      }),
  });

export default Title;
