import options from "options";

const hyprland = await Service.import("hyprland");

const Lang = () =>
  Widget.Button({
    class_names: ["widget", "keyboard"],
    child: Widget.Box({
      spacing: options.spacing_between_symbol_label,
      children: [
        Widget.Label({
          class_name: "symbol",
          label: "",
        }),
        Widget.Label({
          class_name: "text",
          label: "US",
          setup: (self) =>
            self.hook(
              hyprland,
              (self, _, layoutname) => {
                if (!layoutname) return;

                switch (layoutname) {
                  case "English (US)":
                    self.label = "US";
                    break;
                  case "Hebrew":
                    self.label = "HE";
                    break;
                  default:
                    self.label = layoutname;
                    break;
                }
              },
              "keyboard-layout",
            ),
        }),
      ],
    }),
  });

export default Lang;
