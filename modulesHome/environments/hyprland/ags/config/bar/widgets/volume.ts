import options from "options"

const audio = await Service.import("audio");

const Volume = () =>
  Widget.Button({
    class_names: [ "widget", "volume" ],
    on_clicked: () => (audio.speaker.is_muted = !audio.speaker.is_muted),
    on_scroll_up: () => (audio.speaker.volume += 0.05),
    on_scroll_down: () => (audio.speaker.volume -= 0.05),
    child: Widget.Box({
      spacing: options.spacing_between_symbol_label,
      children: [
        Widget.Label({
          class_name: "symbol",
          label: "󰕾",
        }),
        Widget.Label({
          class_name: "text",
        }).hook(audio.speaker, (self) => {
          if (audio.speaker.is_muted) {
            self.label = "MUTED";
            return
          }
          const vol = audio.speaker.volume * 100;

          self.label = `${Math.floor(vol + 0.5)}%`;
        }),
      ],
    }),
  });

export default Volume;
