import options from "options";

const hyprland = await Service.import("hyprland");

const dispatch = (ws: number) =>
  hyprland.messageAsync(`dispatch focusworkspaceoncurrentmonitor ${ws}`);
const Workspaces = (mon: number) =>
  Widget.Box({
    class_names: ["ws", "widget", "box"],
    children: Array.from({ length: 6 }, (_, i) => i + 1).map((i) =>
      Widget.Button({
        attribute: i,
        class_names: ["ws-button"],
        child: Widget.Label({
          class_names: ["symbol"],
          label: `${options.workspace_icons.hasOwnProperty(i) ? options.workspace_icons[i] : i}`,
        }),
        onClicked: () => dispatch(i),
        setup: (self) =>
          self.hook(hyprland, () => {
            // self.visible = hyprland.workspaces.some(ws => ws.id === self.attribute);
            self.toggleClassName(
              "active",
              hyprland.monitors.some(
                (monitor) => monitor.activeWorkspace.id === i,
              ),
            );
            self.toggleClassName(
              "occupied",
              (hyprland.getWorkspace(i)?.windows || 0) > 0,
            );
            self.toggleClassName(
              "monitor-active",
              (hyprland.getMonitor(mon)?.activeWorkspace.id || 0) ===
                self.attribute,
            );
          }),
      }),
    ),
  });

export default Workspaces;
