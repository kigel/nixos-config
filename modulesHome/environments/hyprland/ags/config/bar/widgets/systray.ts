const systemtray = await Service.import("systemtray");

const SysTray = () =>
  Widget.Box({
    class_names: ["systray", "widget", "box"],
    children: systemtray.bind("items").as((i) =>
      i.map((item) =>
        Widget.Button({
          child: Widget.Icon({
            // class_name: "symbol",
          }).bind("icon", item, "icon"),
          tooltipMarkup: item.bind("tooltip_markup"),
          onPrimaryClick: (_, event) => item.activate(event),
          onSecondaryClick: (_, event) => item.openMenu(event),
        }),
      ),
    ),
    visible: systemtray.bind("items").as((i) => i.length !== 0),
  });

export default SysTray;
