export default (monitor: number) =>
  Widget.Window({
    monitor,
    name: `corner${monitor}`,
    class_name: "screen-corners",
    anchor: ["top", "bottom", "right", "left"],
    click_through: true,
    child: Widget.Box({
      class_name: "corner",
      expand: true,
    }),
  });
