import options from "options";
import Lang from "./widgets/lang";
import SysTray from "./widgets/systray";
import Media from "./widgets/media";
import Workspaces from "./widgets/workspaces";
import Volume from "./widgets/volume";
import Time from "./widgets/time";
// import Title from "./widgets/title";

const Bar = (monitor: number) =>
  Widget.Window({
    monitor,
    name: `bar${monitor}`,
    anchor: ["top", "left", "right"],
    exclusivity: "exclusive",
    // class_name: "bar",
    child: Widget.CenterBox({
      class_name: "bar",
      start_widget: Widget.Box({
        class_name: "leftgroup",
        spacing: options.spacing_between_modules,
        hpack: "start",
        children: [Workspaces(monitor), SysTray()],
      }),
      center_widget: Widget.Box({
        class_name: "centergroup",
        spacing: options.spacing_between_modules,
        hpack: "center",
        children: [Time()],
      }),
      end_widget: Widget.Box({
        class_name: "rightgroup",
        spacing: options.spacing_between_modules,
        hpack: "end",
        children: [Media(), Lang(), Volume()],
      }),
    }),
  });

export default Bar;
