import Bar from "bar/bar";
import ScreenCorners from "bar/screen-corners";
import Notifications from "notifications/notifications";
import {mapMonitors} from "lib"

App.config({
  windows: [
    ...mapMonitors(Bar),
    ...mapMonitors(ScreenCorners),
    Notifications(0),
  ],
  style: "./style.css",
});

export {};
