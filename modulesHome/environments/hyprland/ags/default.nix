{ lib, config, inputs, pkgs, ... }:

let cfg = config.kigelHome.programs.ags;
in {
  imports = [ inputs.ags.homeManagerModules.default ];

  options.kigelHome.programs.ags.enable = lib.mkEnableOption "enable ags";


  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [ bun ];

    programs.ags = {
      configDir = ./config;
      enable = true;
    };
  };
}
