{ lib, config, pkgs, inputs, ... }:

let
  cfg = config.kigelHome.environments.hyprland;
  scripts = import ./scripts { inherit pkgs; };

  terminal = config.kigelHome.defaultApps.terminal;
  filemanager = config.kigelHome.defaultApps.filemanager;
  browser = config.kigelHome.defaultApps.browser;
  musicplayer = config.kigelHome.defaultApps.musicplayer;
  discord = config.kigelHome.defaultApps.discord;
  notes = config.kigelHome.defaultApps.notes;

in {
  imports = [ ./waybar ./ags ];

  options.kigelHome.environments.hyprland = {
    enable = lib.mkEnableOption "An entire environment for hyprland";

    monitors = lib.mkOption {
      default = [ ",preferred,auto,auto" ];
      type = lib.types.listOf lib.types.str;
      description = ''
        Monitor settings, look in hyprland wiki.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      # essentials for hyprland
      pkgs.wl-clipboard
      pkgs.hyprpicker
      pkgs.pyprland
    ];

    kigelHome.programs.ags.enable = true;
    # kigelHome.programs.dunst.enable = true;
    # kigelHome.programs.waybar.enable = true;

    wayland.windowManager.hyprland = {
      enable = true;
      # package = inputs.hyprland.packages.${pkgs.system}.hyprland;
      settings = {
        # Monitors
        monitor = cfg.monitors;

        # Programs
        "$terminal" = "${lib.getExe terminal.package}";
        "$fileManager" = "${lib.getExe filemanager.package}";
        "$menu" = "${lib.getExe pkgs.wofi} --show drun";

        "$app1" = "${lib.getExe terminal.package}";
        "$app2" = "${lib.getExe browser.package}";
        "$app3" = "${lib.getExe pkgs.vscode}";
        "$app4" = "${lib.getExe musicplayer.package}";
        # "$app4" = "${lib.getExe pkgs.kitty} ${lib.getExe pkgs.spotify-player}";
        # "$app4" = "${lib.getExe pkgs.kitty} ${lib.getExe pkgs.ncspot}";
        "$app5" = "${lib.getExe discord.package}";
        "$app6" = "${lib.getExe notes.package}";

        # Autostart
        exec-once = [
          # "${lib.getExe pkgs.waybar}"
          "ags -b hypr"
          "${scripts.appworkspace}/bin/appworkspace"
          "${lib.getExe pkgs._1password-gui} --silent"
          "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"
        ];

        # Look And Feel
        general = {
          gaps_in = 5;
          gaps_out = 10;
          border_size = 3;
          resize_on_border = "true";
          allow_tearing = "false";
          layout = "master";
        };
        decoration = {
          rounding = 10;
          active_opacity = "1.0";
          inactive_opacity = "1.0";
          drop_shadow = "false";
          shadow_range = 4;
          shadow_render_power = 3;
          blur = {
            enabled = true;
            size = 20;
            noise = 0;
            xray = true;
          };
        };
        animations = {
          enabled = true;
          bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
          # bezier = "myBezier, 0, 0.5, 0.5, 1.05";
          animation = [
            "windows,            0, 2, myBezier, popin"
            "windowsIn,          1, 2, myBezier, popin"
            "windowsOut,         1, 2, myBezier, popin"
            "windowsMove,        1, 2, myBezier, slide"

            "layers,             0, 2, myBezier, fade"
            "layersIn,           0, 2, myBezier, fade"
            "layersOut,          0, 2, myBezier, fade"

            "fade,               0, 2, myBezier"
            "fadeIn,             1, 2, myBezier"
            "fadeOut,            1, 2, myBezier"
            "fadeSwitch,         1, 2, myBezier"
            "fadeShadow,         0, 2, myBezier"
            "fadeDim,            0, 2, myBezier"
            "fadeLayers,         0, 2, myBezier"
            "fadeLayersIn,       0, 2, myBezier"
            "fadeLayersOut,      0, 2, myBezier"

            "border,             0, 2, myBezier"
            "borderangle,        0, 2, myBezier"

            "workspaces,         1, 2, myBezier, slide"
          ];

        };

        master.new_on_top = false;

        misc = {
          force_default_wallpaper = 0;
          disable_hyprland_logo = true;
        };

        cursor = { hide_on_key_press = true; };

        # Input
        input = {
          kb_layout = "us,il";
          # kb_variant =
          # kb_model =
          kb_options = "grp:win_space_toggle";
          # kb_rules =
          follow_mouse = "1";
          sensitivity = "-0.7"; # -1.0 - 1.0, 0 means no modification.
          touchpad.natural_scroll = false;
        };
        gestures.workspace_swipe = true;

        # Keybindings
        bind = [
          "SUPER, return, exec, $terminal"
          "SUPER, W, killactive,"
          "SUPER, N, exec, $fileManager"
          "SUPER, F, togglefloating,"
          "SUPER, R, exec, $menu"

          "SUPER_SHIFT, Q, exit,"
          "SUPER_SHIFT, R, exec, hyprctl reload && ags -q -b hypr && ags -b hypr"

          "SUPER, J, layoutmsg, cyclenext"
          "SUPER, K, layoutmsg, cycleprev"
          "SUPER, comma, focusmonitor, +1"
          "SUPER, period, focusmonitor, -1"

          "SUPER SHIFT, comma, movewindow, mon:+1"
          "SUPER SHIFT, period, movewindow, mon:-1"

          ", xf86audioraisevolume, exec, ${scripts.volume}/bin/volume up"
          ", xf86audiolowervolume, exec, ${scripts.volume}/bin/volume down"
          ", xf86audiomute, exec, ${scripts.volume}/bin/volume mute"

          "SUPER, xf86audioraisevolume, exec, ${scripts.music}/bin/music next"
          "SUPER, xf86audiolowervolume, exec, ${scripts.music}/bin/music previous"
          "SUPER, xf86audiomute, exec, ${scripts.music}/bin/music play"

          "SUPER, 1, focusworkspaceoncurrentmonitor, 1"
          "SUPER, 2, focusworkspaceoncurrentmonitor, 2"
          "SUPER, 3, focusworkspaceoncurrentmonitor, 3"
          "SUPER, 4, focusworkspaceoncurrentmonitor, 4"
          "SUPER, 5, focusworkspaceoncurrentmonitor, 5"
          "SUPER, 6, focusworkspaceoncurrentmonitor, 6"

          "SUPER SHIFT, 1, movetoworkspace, 1"
          "SUPER SHIFT, 2, movetoworkspace, 2"
          "SUPER SHIFT, 3, movetoworkspace, 3"
          "SUPER SHIFT, 4, movetoworkspace, 4"
          "SUPER SHIFT, 5, movetoworkspace, 5"
          "SUPER SHIFT, 6, movetoworkspace, 6"
          "SUPER SHIFT, 7, movetoworkspace, 7"
          "SUPER SHIFT, 8, movetoworkspace, 8"
          "SUPER SHIFT, 9, movetoworkspace, 9"
          "SUPER SHIFT, 0, movetoworkspace, 10"
        ];
        bindm =
          [ "SUPER, mouse:272, movewindow" "SUPER, mouse:273, resizewindow" ];

        # Windows And Workspaces
        windowrulev2 = [
          "suppressevent maximize, class:.*" # You'll probably like this.

          "float,class:imv"
          "float,class:pcmanfm"

          # so all floating windows start as 50% of the screen size
          "size 50% 50%, class:.*"

          "workspace 2,class:firefox"

          "workspace 3,class:Code"

          "workspace 4,class:Spotify"
          "workspace 4,title:ncspot"

          "workspace 5,class:vesktop"

          "workspace 6,class:obsidian"
        ];
      };
    };

    services.hyprpaper = {
      enable = true;
      settings = {
        ipc = "off";
        splash = true;
        preload = lib.mkForce "${inputs.wallpaper}";
        wallpaper = [ ",${inputs.wallpaper}" ];
      };
    };
  };
}
