{ pkgs, ... }: {
  script = pkgs.writeShellScriptBin "appworkspace" ''
    changebind() {
    	hyprctl keyword unbind SUPER,S
    	case "$1" in
    	1)
    		hyprctl keyword bind "SUPER,S,exec,\$app1"
    		;;
    	2)
    		hyprctl keyword bind "SUPER,S,exec,\$app2"
    		;;
    	3)
    		hyprctl keyword bind "SUPER,S,exec,\$app3"
    		;;
    	4)
    		hyprctl keyword bind "SUPER,S,exec,\$app4"
    		;;
    	5)
    		hyprctl keyword bind "SUPER,S,exec,\$app5"
    		;;
    	6)
    		hyprctl keyword bind "SUPER,S,exec,\$app6"
    		;;
    	7)
    		hyprctl keyword bind "SUPER,S,exec,\$app7"
    		;;
    	8)
    		hyprctl keyword bind "SUPER,S,exec,\$app8"
    		;;
    	9)
    		hyprctl keyword bind "SUPER,S,exec,\$app9"
    		;;
    	*)
    		echo "default"
    		;;
    	esac

    }

    handle() {
    	eventname="$(echo "$1" | sed 's/>>.*//')"
    	if [[ "$eventname" == "focusedmon" ]]; then
    		eventdata="$(echo "$1" | awk -F '>>|,' '{print $3}')"
    		changebind "$eventdata"
    	fi
    	if [[ "$eventname" == "workspace" ]]; then
    		eventdata="$(echo "$1" | sed 's/.*>>//')"
    		changebind "$eventdata"
    	fi
    }

    ${pkgs.socat}/bin/socat -U - UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do handle "$line"; done
  '';
}
