{ pkgs, ... }: {
  appworkspace = (import ./appworkspace.nix { inherit pkgs; }).script;
  volume = (import ./volume.nix { inherit pkgs; }).script;
  music = (import ./music.nix { inherit pkgs; }).script;
}
