{ pkgs, ... }: {
  script = pkgs.writeShellScriptBin "music" ''
    case "$1" in
      "next") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot, spotify_player" next >/dev/null 2>&1 ;;
      "previous") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot, spotify_player" previous >/dev/null 2>&1 ;;
      "play") ${pkgs.playerctl}/bin/playerctl -p "spotify, ncspot, spotify_player" play-pause >/dev/null 2>&1 ;;
    esac
  '';
}
