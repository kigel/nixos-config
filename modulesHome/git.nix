{ config, lib, pkgs, ... }:

let cfg = config.kigelHome.git;
in {
  options.kigelHome.git.enable = lib.mkEnableOption "enable git";

  config = lib.mkIf cfg.enable {
    programs.git = {
      enable = true;
      userName = "kigel";
      userEmail = "itaykigelmain@gmail.com";

      extraConfig = {
        credential = { helper = "store"; };
        pull.rebase = false;
      };
      aliases = {
        s = "!git status && :";
        a = "!git add -A && :";
        c = "!git commit -m \"$1\" && :";
        ac = "!git add -A && git commit -m \"$1\" && :";
        u = "!git pull && git push && :";
      };
    };
  };
}
